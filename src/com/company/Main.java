package com.company;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Main {
    transient int i;
    int flavor;
    Map<Integer, Integer[]> sl = new HashMap<>();

    Main() {


    }

    public static void main(String[] args) {

        Main main = new Main();
        main.setFlavor(3);
        main.checkMoves();
        main.checkSlots();
        main.setFlavor(4);
        main.checkMoves();
        main.checkSlots();

    }

    public void setFlavor(int flavor) {
        sl.clear();
        this.flavor = flavor;
        int move = 1;
        for (int i = 0; i < flavor; i++) {
            for (int j = 0; j < flavor; j++) {
                sl.put(move, new Integer[]{i, j});
                move++;
            }
        }

        System.out.println("\n flavore " + flavor);
    }

    public void checkMoves() {
        System.out.println("\n Check Moves");
        Iterator<Integer> it = sl.keySet().iterator();
        while (it.hasNext()) {
            Integer move = it.next();
            System.out.println("Move : " + move + ", index : " + Arrays.toString(getIndexFromMove(move)));
        }
    }

    public void checkSlots() {
        System.out.println("\n Check slotes");
        Iterator<Integer> it = sl.keySet().iterator();
        while (it.hasNext()) {
            Integer[] index = sl.get(it.next());
            System.out.println("Move : " + getIdFromIndex(index) + ", index : " + Arrays.toString(index));
        }
    }

    public void winner() {
        int min = 0;
        int max = flavor - 1;

        for (int i = 0; i < flavor; i++) {

        }
    }

    public int getIdFromIndex(Integer[] index) {
        return ((flavor * index[0]) + 1) + index[1];
    }

    public Integer[] getIndexFromMove(Integer move) {
        Integer[] index = new Integer[2];
        int div = move - 1;
        if (div == 0) {
            index[0] = 0;
            index[1] = 0;
        } else {
            index[0] = div / flavor;
            index[1] = div % flavor;
        }
        return index;
    }
}
