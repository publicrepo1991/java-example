package com.company.thread;

/**
 * Created by rahul on 13/05/16.
 */
public class SynchronizedNumberThread {
    Integer i = 0;

    /*  public Object Lock1 = new Object();
      public Object Lock2 = new Object();
  */
    public static void main(String args[]) {
        SynchronizedNumberThread s = new SynchronizedNumberThread();
        ThreadDemo1 t1 = new ThreadDemo1(s);
        ThreadDemo2 t2 = new ThreadDemo2(s);
        t1.start();
        /*try {
            t1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static class ThreadDemo1 extends DemoThread {
        private ThreadDemo1(SynchronizedNumberThread numberThread) {
            super(numberThread);
        }
        @Override
        public boolean isValid() {
            return (numberThread.i % 2 == 0);
        }
    }

    private static class ThreadDemo2 extends DemoThread {
        private ThreadDemo2(SynchronizedNumberThread numberThread) {
         super(numberThread);
        }
        @Override
        public boolean isValid() {
            return (numberThread.i % 2 != 0);
        }

        @Override
        public void run() {
            super.run();
        }
    }

    private abstract static class DemoThread extends Thread {
        final SynchronizedNumberThread numberThread;

        private DemoThread(SynchronizedNumberThread numberThread) {
            this.numberThread = numberThread;
        }

        public abstract boolean isValid();

        public void run() {

            while (numberThread.i < 100) {

                    synchronized (numberThread) {
                        if (isValid()) {
                        System.out.println(getName().concat(" : >>>>>>> ") + (numberThread.i++));
                        try {
                            numberThread.notify();
                            numberThread.wait();
                        } catch (InterruptedException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        numberThread.notify();
                    }
                }

            }
        }
    }
}
