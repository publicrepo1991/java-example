package com.company;

/**
 * Created by rahul on 05/02/17.
 */
public class BracesExample {
    public static void main(String[] args) {

        if (isValid("(a+{b+c}-[d-e])[{asda{}]")) {
            System.out.println("Well Formed");
        } else {
            System.out.println("Not Well Formed");
        }
    }

    public static boolean isValid(String braces) {
        braces = FilterBrackets(braces);
        while ((braces.length() != 0) && (braces.contains("[]") || braces.contains("()") || braces.contains("{}"))) {
            //System.out.println(s.length());
            //System.out.println(s);
            braces = braces.replace("[]", "");
            braces = braces.replace("()", "");
            braces = braces.replace("{}", "");
        }

        return braces.length() == 0;
    }

    public static String FilterBrackets(String str) {
        int len = str.length();
        char arr[] = str.toCharArray();
        String filter = "";
        for (int i = 0; i < len; i++) {
            if ((arr[i] == '(') || (arr[i] == ')') || (arr[i] == '[') || (arr[i] == ']') || (arr[i] == '{') || (arr[i] == '}')) {
                filter = filter + arr[i];
            }
        }
        return filter;
    }
}
