package com.company.java8;

/**
 * Created by rahul on 13/05/16.
 */
public class DefaultMethodTest {
    public static void main(String[] strings) {
        Vehicle vehicle = new Car();
        vehicle.print();
    }

    interface Vehicle {
        static void blowHorn() {
            System.out.println("Blowing horn!!!");
        }

        default void print() {
            System.out.println("I am a vehicle!");
        }
    }

    interface FourWheeler {
        default void print() {
            System.out.println("I am a four wheeler!");
        }
    }

    static class Car implements Vehicle, FourWheeler {
        public void print() {
            Vehicle.super.print();
            FourWheeler.super.print();
            Vehicle.blowHorn();
            System.out.println("I am a car!");
        }
    }
}
