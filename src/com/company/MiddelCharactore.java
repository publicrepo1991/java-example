package com.company;

import javax.swing.plaf.TextUI;

/**
 * Created by rahul on 05/02/17.
 */
public class MiddelCharactore {
    public static void main(String[] args) {
        System.out.println("The Sum is " + getMiddle("Rahuly"));
    }

    public static String getMiddle(String word) {
        if (word == null || word.length() == 0) {
            return "";
        }

        int length = word.length();
        int pos = length / 2;
        if (length % 2 == 0) {
            return word.substring(pos - 1, pos + 1);
        }
        return String.valueOf(word.charAt(pos));
    }
}
