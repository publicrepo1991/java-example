package com.company;

import java.util.Arrays;

/**
 * Created by rahul on 05/02/17.
 */
public class WeightSort {

    public static void main(String[] strings) {
        order("103 123 4444 99 2000");
        System.out.println("\n");
        order("2000 10003 1234000 44444444 9999 11 11 22 123");
    }

    public static void order(String strng) {
        String[] values = strng.split(" ");

        int[] newValues = new int[values.length];
        int i = 0;
        for (String value : values) {
            newValues[i++] = sumOfDigit(value);
        }

        System.out.println(strng);
        System.out.println(Arrays.toString(newValues));
        System.out.println(orderWeight(strng));
    }

    public static String orderWeight(String strng) {
        String[] values = strng.split(" ");
       // selectionSort(values);
       // System.out.println(Arrays.toString(values));
        int[] newValues = new int[values.length];
        int i = 0;
        for (String value : values) {
            newValues[i++] = sumOfDigit(value);
        }
        selectionSort(values, newValues);
        return Arrays.toString(values).replace(",", "").replace("[", "").replace("]", "");
    }

    public static void selectionSort(String values[], int[] arr) {

        for (int i = 0; i < arr.length - 1; i++) {
            int index = i;
            for (int j = i + 1; j < arr.length; j++)
                if (arr[j] < arr[index] || (arr[j]==arr[index] && Integer.valueOf(values[j])<Integer.valueOf(values[index])))
                    index = j;

            String tmp = values[index];
            values[index] = values[i];
            values[i] = tmp;

            int smallValue = arr[index];
            arr[index] = arr[i];
            arr[i] = smallValue;
        }
    }


    public static void selectionSort(String values[]) {

        for (int i = 0; i < values.length - 1; i++) {
            int index = i;
            for (int j = i + 1; j < values.length; j++)
                if (Integer.valueOf(values[j]) < Integer.valueOf(values[index]))
                    index = j;

            String tmp = values[index];
            values[index] = values[i];
            values[i] = tmp;
        }
    }


    public static int sumOfDigit(String number) {
        int sum = 0;
        char chs[] = number.toCharArray();
        for (char ch : chs) {
            sum += Character.getNumericValue(ch);
        }
        return sum;
    }
}
